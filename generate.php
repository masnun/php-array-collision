<?php

// Run "php generate.php" > data


$size = pow(2, 16);
$key = 0;
$maxKey = ($size - 1) * $size;

while ($key < $maxKey)
{
    $key += $size;
    echo "{$key}=0&";
}


